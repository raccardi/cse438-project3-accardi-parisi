Authors: Reid Parisi and Roman Accardi, copyright 2019


===============================================
				User Accounts
===============================================

Email: telemonian@gmail.com
Password: password
You will note that the above account has been CLEANING UP.

Email: billy@bob.com
Password: monkey
You will notice that this account has not been as much.

And many more accounts available upon request

===============================================
					Notes
===============================================

First, as emulator issues abound, the application is optimized
for display on a Samsung Galaxy J3 Star; this device has a screen
size of approximately four inches.

It should also be noted that, in between each game, the user is
taken to another activity where they then have the option to begin
a new game, rather than immediately beginning a new game as specified.
This was a design choice consistent with our creative project (see
below).

Also, password length needs to be 6 or more.  You probably knew that,
but we both ran into a lot of authentication failures before we
figured that out.

Also, the dealer sometimes hits on 17 if it's a "soft" 17, viz., if
one of their cards is an ace.  This was part of an effort to slightly
improve the dealer's AI.

===============================================
				Creative Portion
===============================================

Our creative portion involved strategic implementation of funds
associated with an account, which are then bet on each game.

Upon account creation, a user is given $100 in virtual currency
(not yet redeemable for USD).  The "high scores" are tracked based
on who has the most money rather than win/loss ratio.

Further, before each game, the user is prompted to enter some
nonnegative integer less than or equal to their net worth.
If they win the game, they gain the specified amount of money;
if they lose, they lose that amount of money, as might be
expected.

After a game completes, the user is taken to a screen where they
have the option to either play again or return to the Dashboard.
If they play again, they may enter a new amount of money to bet,
circumventing the dialog window, which saves a few frames for any
Blackjack speedrunners who may be using the app.

(10 points) Player can login and login data is stored in Firebase
(10 points) Player’s win/loss counts are displayed on startup
(10 points) The Player receives two cards face up, and the dealer receives one card face up and one card face down
(5 points) Swiping right allows the Player to hit
(5 points) Double tapping allows the Player to stand
(10 points) Cards being dealt are animated, and all cards are visible.
(5 points) If the Player goes over 21, they automatically bust
(10 points) The dealer behaves appropriately based on the rules described above
(10 points) Once the game is complete, the winner should be declared and the Firebase database should be updated appropriately
(5 points) A new round is automatically started after each round
(5 points) Player can logout
(10 points) A leaderboard is shown in a separate tab or activity and is consistent among installations of the app
(15 points) Creative portion!

This looks great guys! I tested it out and it all works perfectly. Just some minor suggestions: the layout could use a little cleaning up, but overall it is pretty good. I also thought the animations were a bit slow and the cards were a bit small.

As a speedrunner myself (who speedruns blackjack though?) I did appreciate the ability to bypass the betting menu.

Total: 110 / 100

