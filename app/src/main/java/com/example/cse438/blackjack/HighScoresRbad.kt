package com.example.cse438.blackjack

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_high_scores.*

class HighScoresRbad : AppCompatActivity() {

    private var adapter = HomeAdapter()

    private lateinit var db: FirebaseFirestore

    //will be retrieved from the intent
    private lateinit var username: String
    private var userScoresData: ArrayList<Pair<String,Long>> =ArrayList()

    //path to the "high scores" and "users" collections in the database
    companion object DatabasePaths {
        const val highScoresPath = "highScores"
        const val usersPath = "users"
    }

    private val TAG = "Dashboard"

    //the maximum number of high scores to display; optional
    private val MAX_DISPLAY = 3


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_high_scores)

        //set up the firestore
        db = FirebaseFirestore.getInstance()
        //this will be useful in retrieving high scores from the database

        //retrieve the username from the intent
        username = intent.getStringExtra("username")

        //if the user clicks back brings them back to dashboard
        findViewById<Button>(R.id.backToDashboard).setOnClickListener {
            val intent = Intent(this, Dashboard::class.java)
            intent.putExtra("username", username)
            startActivity(intent)
        }

        //gets the users score and updates the textedit with their score
        db.collection(Dashboard.usersPath)
            .document(username)
            .get()
            .addOnSuccessListener {snapshot ->
                val dataMap = snapshot.data
                findViewById<TextView>(R.id.yourScore).text = "your score: $"+dataMap?.get("moneyCount").toString()
            }
            .addOnFailureListener {
                Log.e(TAG, "Could not retrieve score data")
            }

        //gets the scores of all users and adds them to the
        // array list member variable
        db.collection(Dashboard.usersPath)
            .get()  //hi
            .addOnSuccessListener {documents ->
                //searches through every user
                for (document in documents) {
                    //constructs a pair of their username and money count
                    val dataPair = Pair(document.id, document["moneyCount"] as Long)
                    userScoresData.add(dataPair)
                    Log.d(TAG, document.id + document.toString())
                    Log.d(TAG, "${document.id} => ${document.data}")
                }


                //a custom function to very inefficiently sort the data
                sortList()


                //OPTIONAL: Restrict display to just the top N scores
                /*if (MAX_DISPLAY < userScoresData.size) {
                    //this was being unfriendly
                    //userScoresData = userScoresData.subList(0, MAX_DISPLAY) as ArrayList<Pair<String, Long>>

                    //probably the most horrific solution to this problem possible
                    var tempList = ArrayList<Pair<String, Long>>()
                    for (i: Int in 0 until MAX_DISPLAY) {
                        tempList.add(userScoresData[i])
                    }
                    userScoresData = tempList
                }*/
                adapter.notifyDataSetChanged()
            }
            .addOnFailureListener {
                Log.e(TAG, "Could not retrieve score data")
            }

        recyclerView.layoutManager = LinearLayoutManager(this@HighScoresRbad)
        recyclerView.adapter=adapter
        val observer= Observer<HashMap<String, Any>>{
            recyclerView.adapter=adapter
            //how to see if items are different
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    if (p0 >= userScoresData.size || p1 >= userScoresData.size) {
                        return false;
                    }
                    return (userScoresData[p0].first == userScoresData[p1].first) && (userScoresData[p0].second == userScoresData[p1].second)
                }

                override fun getOldListSize(): Int {
                    return userScoresData.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return userScoresData[p0] == userScoresData[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            userScoresData = it as ArrayList<Pair<String,Long>>
        }
    }

    //function to sort the list of scores
    private fun sortList() {
        //bubble sort babyyyy
        for (i: Int in 0 until userScoresData.size) {
            var maxLoc = i
            for (j: Int in i until userScoresData.size) {
                if (userScoresData[j].second > userScoresData[maxLoc].second) {
                    maxLoc = j
                }
            }
            val temp = userScoresData[i]
            userScoresData[i] = userScoresData[maxLoc]
            userScoresData[maxLoc] = temp
        }
    }


    //recycler view adapter
    inner class HomeAdapter : RecyclerView.Adapter<HomeAdapter.JokeViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): JokeViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.highscore_frag_elem, p0, false)
            return JokeViewHolder(itemView)
        }

        //what to do with each element
        override fun onBindViewHolder(p0: JokeViewHolder, p1: Int) {
            //gets joke aka player from the player and fills the fields of the itemView
            //with the player data from the array
            val joke = userScoresData[p1]
            p0.highScore.text = joke.second.toString()
            p0.username.text = (p1+1).toString()+": "+joke.first

        }


        override fun getItemCount(): Int {
            return userScoresData.size
        }


        inner class JokeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var highScore: TextView = itemView.findViewById(R.id.hs_score)
            var username: TextView = itemView.findViewById(R.id.hs_user)
            var row = itemView

        }
    }
}


