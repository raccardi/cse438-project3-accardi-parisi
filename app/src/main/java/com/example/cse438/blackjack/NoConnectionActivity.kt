package com.example.cse438.blackjack

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class NoConnectionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_no_connection)
    }
}
