package com.example.cse438.blackjack

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class ResultsPage : AppCompatActivity() {

    private lateinit var username: String
    private var playerMoney: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results_page)

        //get all this nonsense from the intent
        username = intent.getStringExtra("username")
        playerMoney = intent.getIntExtra("money", 0)
        val playerPoints: Int = intent.getIntExtra("playerPoints", 0)
        val dealerPoints: Int = intent.getIntExtra("dealerPoints", 0)
        val result: String = intent.getStringExtra("result")

        //update the text displays accordingly
        findViewById<TextView>(R.id.player_points).text = playerPoints.toString()
        findViewById<TextView>(R.id.dealer_points).text = dealerPoints.toString()
        findViewById<TextView>(R.id.result).text = result
        findViewById<TextView>(R.id.player_money).text = "$" + playerMoney.toString()

        //onclick listener for returning to dashboard or playing again
        findViewById<Button>(R.id.dashboard).setOnClickListener {
            val intent = Intent(this, Dashboard::class.java)
            //all we need is to give the dashboard the username and send it on its merry way
            intent.putExtra("username", username)
            startActivity(intent)
        }

        //start another game
        //this one is a bit harder, as we need to take whatever the new bet
        // amount is and do something with that
        findViewById<Button>(R.id.rematch).setOnClickListener {
            //see if we can get something from the bet entry field
            var somethingEntered = true
            var newBet: Int = 0
            try {
                val newBetString: String = findViewById<EditText>(R.id.bet).text.toString()
                newBet = newBetString.toInt()
            } catch (e: NumberFormatException) {
                somethingEntered = false
            }

            if (newBet > playerMoney) {
                val toast = Toast.makeText(this, "Insufficient funds", Toast.LENGTH_SHORT)
                toast.show()
            }
            else {
                val intent = Intent(this, Play::class.java)
                intent.putExtra("username", username)

                //if there was something in the bet field, go ahead and enter it
                if (somethingEntered) {
                    intent.putExtra("bet", newBet)
                }
                startActivity(intent)
            }

        }


    }
}
