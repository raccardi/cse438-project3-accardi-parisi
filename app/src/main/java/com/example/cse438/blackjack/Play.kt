package com.example.cse438.blackjack

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import com.example.cse438.blackjack.Dashboard.DatabasePaths
import com.example.cse438.blackjack.util.CardRandomizer
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

import android.support.constraint.ConstraintLayout
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.ActionBar
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.ImageView.*
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.dialoginput.*
import kotlinx.android.synthetic.main.activity_play.betID
import kotlin.collections.HashMap
import kotlin.math.max
import kotlin.math.min
import android.support.v4.os.HandlerCompat.postDelayed




class Play : AppCompatActivity() {





    //path to the "high scores" and "users" collections in the database
    companion object DatabasePaths {
        const val highScoresPath = "highScores"
        const val usersPath = "users"
    }
    private lateinit var db: FirebaseFirestore




    //player and dealer global vars
    private var bet: Int=-1
    private var playerMoney: Int?=0
    private var playerWins: Int? =0
    private var playerLosses: Int?=0
    private var playerNumCards=0
    private var dealerNumCards=0
    private var dealerNumAces=0
    private var playerNumAces=0
    private var playerJack=0 //to check for blackJack
    private var playerPoints=0 //points are their cards values summed besides aces
    private var dealerPoints=0
    private var betProvided = false
    private lateinit var mDetector: GestureDetectorCompat

    //will be retrieved from the intent
    private lateinit var username: String

    private val TAG = "Play"
    private val duration: Long = 3000

    //we need to keep a set of current running animators
    // so that we can wait for each one to finish before starting
    // another one
    private var animatorSetSet = HashSet<AnimatorSet>()

    //variables used to facilitate random card generation
    private lateinit var randomizer: CardRandomizer
    private lateinit var cardList: ArrayList<Int>
    private lateinit var rand: Random



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play)

        //set up everything necessary for random card generation
        randomizer = CardRandomizer()
        cardList = randomizer.getIDs(this) as ArrayList<Int>
        rand = Random()

        //set up the firestore
        db = FirebaseFirestore.getInstance()

        //this will be useful in adding scores to the database!

        //retrieve the username from the intent
        username = intent.getStringExtra("username")
        //see if the intent also includes a field for bet amount
        // if so, no need to prompt
        if (intent.hasExtra("bet")) {
            //just get the bet from the intent
            bet = intent.getIntExtra("bet", 0)

            betID.text="you bet $"+bet.toString()
            betProvided = true

            firstCards()
        }
        Log.e(TAG, "here")

        //motion detector
        mDetector = GestureDetectorCompat(this, MyGestureListener())


        //query the Firestore database based on the username
        // and get the current money count, wins, and losses  for the player

        db.collection(Dashboard.DatabasePaths.usersPath).document(username).get()
            .addOnSuccessListener {snapshot ->
                Log.e(TAG,"datamap not being set")
                val dataMap = snapshot.data
                var winsLongToInt:Long = dataMap?.get("winCount") as Long
                var lossesLongToInt:Long = dataMap?.get("lossCount") as Long
                var longToInt:Long = dataMap?.get("moneyCount") as Long
                playerWins=winsLongToInt.toInt()
                playerLosses=lossesLongToInt.toInt()
                playerMoney=longToInt.toInt()

                //query user for the bet information if that has not already
                // been retrieved from the intent
                if (!betProvided) {
                    displayDialog(R.layout.dialoginput) //gets the user bet for input
                }

                //just to double-check
                if (bet > playerMoney as Int) {
                    bet = 0
                }


            }
            .addOnFailureListener {exception->
                Log.e(TAG, "Could not retrieve score data",exception)
            }


        //set up a quick on-click listener to terminate the current
        // activity and go back to the Dashboard
        findViewById<Button>(R.id.dashboard).setOnClickListener {
            val intent = Intent(this, Dashboard::class.java)
            intent.putExtra("username", username)
            startActivity(intent)
        }
    }

    private fun displayDialog(layout: Int){ //displays the popup to ask for money input
        var moneyInt: Int = playerMoney as Int
        val dialog = Dialog(this)
        dialog.setContentView(layout)
        val window = dialog.window
        window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        dialog.body.text= "bet money with max bet "+moneyInt.toString()
        dialog.findViewById<Button>(R.id.close).setOnClickListener {
            val text=dialog.totalinput.text.toString()
            var num:Int=0
            var err = true                     // err is false if there is an error in input
            try {
                num =text.toInt()
            } catch (e: NumberFormatException) {
                err = false
            }
            if(err&&num<=moneyInt&&num>=0){
                bet=num
                Log.d("Android:",text)
            }

            if(bet!=-1) {
                betID.text="you bet $"+bet.toString()
                dialog.dismiss()
            }

            //then deal the first cards
            firstCards()

        }
        dialog.show()
    }

    override fun onTouchEvent(event: MotionEvent) : Boolean {
        mDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    private inner class MyGestureListener : GestureDetector.SimpleOnGestureListener() {
        private var swipedistance = 150

//on double tap the user stays and it is then the dealers turn
    override fun onDoubleTap(e: MotionEvent?): Boolean {
            val toast = Toast.makeText(this@Play, "you stayed", Toast.LENGTH_SHORT)
            toast.show()
            dealerTurn()
            return true
        }

        //on swipe right the user draws a card and if they bust its the dealers turn else they go again
        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {

             if (e2.x - e1.x > swipedistance) {
                 playerDrawCard(playerNumCards+1)
                 if(playerPoints+playerNumAces>21){
                     val toast = Toast.makeText(this@Play, "you busted", Toast.LENGTH_SHORT)
                     toast.show()
                     dealerTurn()
                 }
                 return true
             }
            return false
        }

    }

    //deals the first cards necessary to begin the game
    private fun firstCards() {
        //draw the first two cards for the palyer and dealer (dealers first is face down)
        playerDrawCard(1)
        Handler().postDelayed({
            playerDrawCard(2)
            Handler().postDelayed({
                dealerDrawCard(1)
                Handler().postDelayed({
                    dealerDrawCard(2)
                }, duration)
            }, duration)
        }, duration)
        if(playerJack==1&&playerNumAces==1&&playerNumCards==2){
            playerWins=playerWins!!+1
            playerMoney=playerMoney!!+2*bet


            addScore()
            val intent = Intent(this, ResultsPage::class.java)
            intent.putExtra("username", username)
            intent.putExtra("playerPoints", playerPoints)
            intent.putExtra("dealerPoints", dealerPoints)
            intent.putExtra("money", playerMoney as Int)
            intent.putExtra("result", "BLACKJACK!")
            startActivity(intent)
//                    val toast = Toast.makeText(this@Play, "BLACKJACK!!", Toast.LENGTH_SHORT)
//                    toast.show()
        }
    }

//player draws a card and adds to his points total and number of cards
    private fun playerDrawCard(i: Int){
        playerNumCards++
        var r: Int = rand.nextInt(cardList.size)
        var id: Int = cardList.get(r)
        var name: String = resources.getResourceEntryName(id)
        name+=".png"
        var cardValue=getVal(name)
        if(cardValue==-2){
            throw error("card value not read")
        }
        else if(cardValue==-1){
            playerNumAces++
        }
        else{
            if(name.contains("Jack",true)){
                playerJack++
            }
            playerPoints+=cardValue
        }
        val constraintLayout = findViewById(R.id.constraintLayout) as ConstraintLayout
        var imageView = ImageView(this)
        imageView.setImageResource(id)


        //val params = imageView.getLayoutParams()
        val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        params.height=168
        params.width=102
        //display the side by side at the mid bottum of the screen
        val x_co: Float = ((102+3) * (i-1)).toFloat()
        val y_co: Float = ((700)).toFloat()
        //imageView.setX(x_co)
        //imageView.setY(y_co)
        imageView.setLayoutParams(params)

        //starts moving the card and adds the animator set
        // used to do so to our set of animator sets
        animatorSetSet.add(moveTo(imageView,x_co,y_co))

        constraintLayout.addView(imageView)
    }
    //player draws a card and adds to his points total and number of cards
    private fun dealerDrawCard(i: Int){
        dealerNumCards++
        var r: Int = rand.nextInt(cardList.size)
        var id: Int = cardList.get(r)
        var name: String = resources.getResourceEntryName(id)
        name+=".png"
        var cardValue=getVal(name)
        if(cardValue==-2){
            throw error("card value not read")
        }
        else if(cardValue!=-1 && dealerNumCards!=1){
            dealerPoints+=cardValue
        }
        else if(dealerNumCards!=1){
            dealerNumAces++
        }

        if(i==1&&dealerNumCards==1){
            id=R.drawable.back
        }
        else if(i==1&&dealerNumCards!=1){
            dealerNumCards-- //to account for dealing over the facedown card
        }

        val constraintLayout = findViewById(R.id.constraintLayout) as ConstraintLayout
        var imageView = ImageView(this)
        imageView.setImageResource(id)



        val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        params.height=168
        params.width=102

        val x_co: Float = ((102+3) * (i-1)).toFloat()
        val y_co: Float = ((300)).toFloat()
        //imageView.setX(x_co)
        //imageView.setY(y_co)
        imageView.setLayoutParams(params)

        //before we start animating another card, first we have
        // to wait for all of the previously started animations
        // to complete
        //waitForAllAnimations()

        //starts moving the card and adds the AnimatorSet to the
        // set of animator sets so we can be sure to finish them all
        animatorSetSet.add(moveTo(imageView,x_co,y_co))

        constraintLayout.addView(imageView)
    }

    //logic for deciding if dealer should draw or or stay
    private fun dealerTurn() {
        dealerDrawCard(1)

        Handler().postDelayed({
            //handlers are apparently very useful for repeated calls and delays
            // as we are going for here
            val handler = Handler()
            val dealerTurnRunnable = object : Runnable {
                //this runnable executes the dealer logic, waiting for 3 seconds
                // between each call in order to
                override fun run() {
                    //Log.d("Handlers", "Called on main thread")
                    if(dealerNumAces==0) {
                        if (dealerPoints > 17) {
                            //handler.removeCallbacks(this)
                            resolveGame(handler, this)
                            return
                        }
                        else {
                            dealerDrawCard(dealerNumCards+1)
                            val toast = Toast.makeText(this@Play, "dealer hit", Toast.LENGTH_SHORT)
                            toast.show()
                            //not quite sure
                            handler.postDelayed(this, duration)
                        }
                    }
                    else {
                        var temp=dealerPoints+dealerNumAces-1
                        if(temp<7||(temp<16&&temp>10)) {
                            dealerDrawCard(dealerNumCards+1)
                            //not quite sure
                            handler.postDelayed(this, duration)
                        }
                        else{
                            val toast = Toast.makeText(this@Play, "dealer stayed", Toast.LENGTH_SHORT)
                            toast.show()
                            //handler.removeCallbacks(this)
                            resolveGame(handler, this)
                            return
                        }
                    }


                }
            }
            handler.post(dealerTurnRunnable)
        }, duration)


//        while (true){
//            if(dealerNumAces==0) {
//                if (dealerPoints > 17) {
//                    resolveGame()
//                    return
//                }
//                else {
//                    dealerDrawCard(dealerNumCards+1)
//                    val toast = Toast.makeText(this@Play, "dealer stayed", Toast.LENGTH_SHORT)
//                    toast.show()
//                    continue
//                }
//            }
//            else {
//                var temp=dealerPoints+dealerNumAces-1
//                if(temp<7||(temp<17&&temp>10)) {
//                    dealerDrawCard(dealerNumCards+1)
//                    continue
//                }
//                else{
//                    val toast = Toast.makeText(this@Play, "dealer stayed", Toast.LENGTH_SHORT)
//                    toast.show()
//                    resolveGame()
//                    return
//                }
//            }
//        }

    }

    //end of game logic to decide who won and update the wins, losses, and money accordingly
    private fun resolveGame(handler: Handler, runnable: Runnable) {
        handler.removeCallbacks(runnable)

        //calculate dealer points
        if(max(dealerPoints+dealerNumAces,dealerPoints+dealerNumAces+10)<=21){
            dealerPoints=max(dealerPoints+dealerNumAces,dealerPoints+dealerNumAces+10)
        }
        else{
            dealerPoints= min(dealerPoints+dealerNumAces,dealerPoints+dealerNumAces+10)
        }
        //calculate player points
        if(max(playerPoints+playerNumAces,playerPoints+playerNumAces+10)<=21){
            playerPoints=max(playerPoints+playerNumAces,playerPoints+playerNumAces+10)
        }
        else{
            playerPoints=min(playerPoints+playerNumAces,playerPoints+playerNumAces+10)
        }

        //now, we move onto the results page activity
        val intent = Intent(this, ResultsPage::class.java)
        intent.putExtra("username", username)
        intent.putExtra("playerPoints", playerPoints)
        intent.putExtra("dealerPoints", dealerPoints)

        if(playerPoints>21&& dealerPoints>21){
//            val toast = Toast.makeText(this@Play, "its a push", Toast.LENGTH_SHORT)
//            toast.show()
            intent.putExtra("result", "It's a push!")
        }
        else if(dealerPoints>21){
//            val toast = Toast.makeText(this@Play, "dealer busted", Toast.LENGTH_SHORT)
//            toast.show()
            intent.putExtra("result", "Dealer busted")

            playerMoney=playerMoney!!+bet
            playerWins=playerWins!!+1
            addScore()

        }
        else if(playerPoints>21){
//            val toast = Toast.makeText(this@Play, "player busted", Toast.LENGTH_SHORT)
//            toast.show()
            intent.putExtra("result", "Player busted")

            playerMoney=playerMoney!!-bet
            playerLosses=playerLosses!!+1
            addScore()
        }
        else if(dealerPoints>playerPoints){
//            val toast = Toast.makeText(this@Play, "dealer wins", Toast.LENGTH_SHORT)
//            toast.show()
            intent.putExtra("result", "Dealer wins")

            playerMoney=playerMoney!!-bet
            playerLosses=playerLosses!!+1
            addScore()
        }
        else if(playerPoints>dealerPoints){
//            val toast = Toast.makeText(this@Play, "player wins", Toast.LENGTH_SHORT)
//            toast.show()
            intent.putExtra("result", "Player wins")

            playerMoney=playerMoney!!+bet
            playerWins=playerWins!!+1
            addScore()
        }
        else{
//            val toast = Toast.makeText(this@Play, "its a push", Toast.LENGTH_SHORT)
//            toast.show()
            intent.putExtra("result", "It's a push!")
        }
        intent.putExtra("money", playerMoney as Int)
        //now we go ahead and navigate onto the results page
        startActivity(intent)
    }

    //animates the card from the top left corner to a given location
    //returns the animator set used in the process
    fun moveTo(img: ImageView,targetX: Float, targetY: Float): AnimatorSet {


        val animSetXY = AnimatorSet()
        val x = ObjectAnimator.ofFloat(
            img,
            "translationX",
            img.translationX,
            targetX
        )

        val y = ObjectAnimator.ofFloat(
            img,
            "translationY",
            img.translationY,
            targetY
        )
        animSetXY.playTogether(x, y)
        animSetXY.duration = duration
        animSetXY.start()

        return animSetXY

    }


    /**
     * A pretty simple function that adds a score, money, wins, and losses to the
     * firestore database
     */
    private fun addScore() {
        //I can't think of any real reason why we would want
        // each score to be indexed by anything in particular
        //So this just adds the score as a new document with
        // an arbitrary ID and the appropriate information in
        // the email and score fields
        //this was useful: https://firebase.google.com/docs/firestore/manage-data/add-data


        val newGameInfo = HashMap<String,Any>()
        newGameInfo["lossCount"]=playerLosses!!.toLong()
        newGameInfo["winCount"]=playerWins!!.toLong()
        newGameInfo["moneyCount"]=playerMoney!!.toLong()

        db.collection(Dashboard.DatabasePaths.usersPath).document(username).set(newGameInfo)
            .addOnSuccessListener {
                Log.d(TAG,"new game info added successfully")
            }
            .addOnFailureListener{
                Log.d(TAG, "new game info could not be added")
            }
    }

    /**
     * Returns the ID of a random card, but this is almost
     * entirely taken from the assignment description
     */
    private fun getCardId() : Int {
        val r = rand.nextInt()
        return cardList.get(r)
    }

    //returns the value of a given string if its contains a card value
    private fun getVal(name: String):Int{
        if(name.contains("ace".toRegex())){
            return -1 //as is -1 because it could take two value 1 or 11
        }
        if(name.contains('2',true)){
            return 2
        }
        if(name.contains('3',true)){
            return 3
        }
        if(name.contains('4',true)){
            return 4
        }
        if(name.contains('5',true)){
            return 5
        }
        if(name.contains('6',true)){
            return 6
        }
        if(name.contains('7',true)){
            return 7
        }
        if(name.contains('8',true)){
            return 8
        }
        if(name.contains('9',true)){
            return 9
        }
        if(name.contains("10".toRegex())){
            return 10
        }
        if(name.contains("jack".toRegex())){
            return 10
        }
        if(name.contains("queen".toRegex())){
            return 10
        }
        if(name.contains("king".toRegex())){
            return 10
        }
        return -2
    }

    //this function is sort of a poor-man's finish statement
    //waits for all of the animations to stop
    private fun waitForAllAnimations() {
        for (animSet in animatorSetSet) {
            //wait for it to stop
            //in this weird-as-heck while loop
            while (animSet.isRunning());
            animatorSetSet.remove(animSet)
        }
    }
}
