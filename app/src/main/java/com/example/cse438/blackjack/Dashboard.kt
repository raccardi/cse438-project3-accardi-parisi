package com.example.cse438.blackjack

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.firebase.firestore.FirebaseFirestore

/**
 * This is the screen that the user will see on first logging
 * into the app
 * Displays high scores, gives the player option to
 * start a new game or to view a list of the high scores
 * across the app
 * User can also log out
 */

class Dashboard : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore

    //will be retrieved from the intent
    private lateinit var username: String

    //path to the "high scores" and "users" collections in the database
    companion object DatabasePaths {
        const val highScoresPath = "highScores"
        const val usersPath = "users"
    }

    private val TAG = "Dashboard"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        //set up the firestore
        db = FirebaseFirestore.getInstance()
        //this will be useful in retrieving high scores from the database

        //retrieve the username from the intent
        username = intent.getStringExtra("username")
        findViewById<TextView>(R.id.usernameField).text="user: "+username

        //set up the logout click listener
        //this is a bit roundabout, but essentially we pass everything back
        // to the login activity, adding an extra instructing the login activity
        // to handle all of the messy authentication stuff with logging out.
        findViewById<Button>(R.id.logout).setOnClickListener {
            val intent = Intent(this, loginActivityR::class.java)
            intent.putExtra("logout", true)
            startActivity(intent)
        }
        findViewById<Button>(R.id.start_game).setOnClickListener {
            val intent = Intent(this, Play::class.java)
            intent.putExtra("username", username)
            startActivity(intent)
        }
        findViewById<Button>(R.id.high_scores).setOnClickListener {
            val intent = Intent(this, HighScoresRbad::class.java)
            intent.putExtra("username", username)
            startActivity(intent)
        }
        //query the Firestore database based on the username
        // and set the win-loss counter appropriately and moneyC
        db.collection(usersPath)
            .document(username)
            .get()
            .addOnSuccessListener {snapshot ->
                val dataMap = snapshot.data
                findViewById<TextView>(R.id.win_count).text = dataMap?.get("winCount").toString()
                findViewById<TextView>(R.id.loss_count).text = dataMap?.get("lossCount").toString()
                findViewById<TextView>(R.id.money_count).text = "$"+dataMap?.get("moneyCount").toString()
            }
            .addOnFailureListener {
                Log.e(TAG, "Could not retrieve score data")
            }
    }
}
