package com.example.cse438.blackjack

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
//import com.google.firebase.quickstart.auth.R
import kotlinx.android.synthetic.main.activity_login.*

class loginActivityR : AppCompatActivity(), View.OnClickListener {

    // [START declare_auth]
    private lateinit var auth: FirebaseAuth
    // [END declare_auth]

    //when setting up user accounts, need to use database
    private lateinit var db: FirebaseFirestore


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)



        // Buttons

        email_sign_in_button.setOnClickListener(this)
        email_create_account_button.setOnClickListener(this)
        //signOutButton.setOnClickListener(this)
        //verifyEmailButton.setOnClickListener(this)
        // [START initialize_auth]
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
        // [END initialize_auth]

        db = FirebaseFirestore.getInstance()
    }

    // [START on_start_check_user]
    public override fun onStart() {
        super.onStart()

        //see if this activity was started with the goal
        // of signing out--if so, sign out
        if (intent.hasExtra("logout")) {
            Log.d(TAG, "user signed out")
            signOut()
        }

        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    // [END on_start_check_user]
    private fun createAccount(email: String, password: String) {


        Log.d(TAG, "createAccount:$email")
        if (!validateForm()) {
            return
        }

        // showProgressDialog() //maybe put back in

        // [START create_user_with_email]
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")

                    //add the new user to the database
                    addUser(email)

                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(null)
                }

                // [START_EXCLUDE]
                // hideProgressDialog() //maybe put back in
                // [END_EXCLUDE]
            }
        // [END create_user_with_email]
    }

    /**
     * Upon creating a new user account, adds that account
     * to the firestore database, initializing win and loss
     * count to 0
     */
    private fun addUser(email: String) {
        // add user to database, indexed by email, including win and loss count (both 0)
        val newUserInfo = HashMap<String, Any>()
        newUserInfo["winCount"] = 0
        newUserInfo["lossCount"] = 0
        newUserInfo["moneyCount"]=100

        //users are indexed by their email
        //so we create a new document with the email as the ID
        //NOTE: overrides any previous account data that the
        // user might have
        db.collection(Dashboard.usersPath).document(email)
            .set(newUserInfo)
            .addOnSuccessListener {
                Log.d(TAG, "New user added successfully")
            }
            .addOnFailureListener {
                Log.d(TAG, "New user could not be added")
            }
    }

    private fun signIn(email: String, password: String) {
        Log.d(TAG, "signIn:$email")
        if (!validateForm()) {
            return
        }

        // showProgressDialog() //maybe put back in

        // [START sign_in_with_email]
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(null)
                }

                // [START_EXCLUDE]
                // if (!task.isSuccessful) {
                //   status.setText(R.string.auth_failed)
                //}
                //  hideProgressDialog() //maybe put back in
                // [END_EXCLUDE]
            }
        // [END sign_in_with_email]
    }

    private fun signOut() {
        auth.signOut()
        updateUI(null)
    }


    private fun validateForm(): Boolean {
        var valid = true

        val email2 = email.text.toString()
        if (TextUtils.isEmpty(email2)) {
            email.error = "Required."
            valid = false
        } else {
            email.error = null
        }

        val password2 = password.text.toString()
        if (TextUtils.isEmpty(password2)) {
            password.error = "Required."
            valid = false
        } else {
            password.error = null
        }
        return valid
    }

    private fun updateUI(user: FirebaseUser?) {
        // hideProgressDialog() //maybe put back in
        if (user != null) {
//                status.text = getString(R.string.emailpassword_status_fmt,
//                    user.email, user.isEmailVerified)
//                detail.text = getString(R.string.firebase_status_fmt, user.uid)
//                emailPasswordButtons.visibility = View.GONE
//                emailPasswordFields.visibility = View.GONE
//                signedInButtons.visibility = View.VISIBLE
//                verifyEmailButton.isEnabled = !user.isEmailVerified
            //proceed onto the dashboard
            val intent = Intent(this, Dashboard::class.java)
            intent.putExtra("username", user?.email)
            startActivity(intent)
        } else {
//                status.setText(R.string.signed_out)
//                detail.text = null
//
//                emailPasswordButtons.visibility = View.VISIBLE
//                emailPasswordFields.visibility = View.VISIBLE
//                signedInButtons.visibility = View.GONE
        }
    }

    override fun onClick(v: View) {
        val i = v.id
        when (i) {
            R.id.email_create_account_button -> createAccount(email.text.toString(), password.text.toString())
            R.id.email_sign_in_button -> signIn(email.text.toString(), password.text.toString())
            //R.id.signOutButton -> signOut()
            //R.id.verifyEmailButton -> sendEmailVerification()
        }
    }

    companion object {
        private const val TAG = "EmailPassword"
    }
}

